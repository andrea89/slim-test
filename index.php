<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/classes/DB.php';

$app = new Slim\App;

$app->get('/', function ($request, $response) {
	return 'hello world';
});

$app->get('/users', function ($request, $response) {
	$data = new DB();
	$sql = 'SELECT * FROM `user` LIMIT 5';
	$data->query($sql);

	if( $data->num_rows() > 0 )
		return json_encode($data->fetch_array());
	else
		return 'no user found';

	$data->close();
});

$app->post('/users', function ($request, $response) {

	if( !isset($_POST['firstName']) || !isset($_POST['lastName']) || !isset($_POST['email']) || !isset($_POST['password'])){
		return 'Please provide all the mandatory fields';
	}
	else{

		$firstName = $_POST['firstName'];
		$lastName = $_POST['lastName'];
		$email = $_POST['email'];
		$password = $_POST['password'];

		$data = new DB();
		$sql = "INSERT INTO `user`(`firstName`, `lastName`, `email`, `password`) VALUES ('$firstName', '$lastName', '$email', '$password')";

		if( $data->query($sql) ) return 'Data inserted';
		else return 'An error occurred';

		$data->close();

	}
});

$app->put('/users', function ($request, $response) {

	if( !isset($_POST['firstName']) || !isset($_POST['lastName']) || !isset($_POST['email']) || !isset($_POST['password']) || !isset($_POST["old_email"])){
		return 'Please provide all the mandatory fields';
	}
	else{

		$firstName = $_POST['firstName'];
		$lastName = $_POST['lastName'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$old_email = $_POST["old_email"];

		$data = new DB();
		$sql = "update user set firstName='$firstName', lastName='$lastName', email='$email', password='$password' where email='$old_email'";

		if( $data->query($sql) ) return 'Data updated';
		else return 'An error occurred';

		$data->close();

	}
});

$app->run();

?>